#!/usr/bin/env sh

# Run kubeadm reset
ssh -t nimbus "sudo kubeadm reset"

# Remove kubeconfig
ssh -t nimbus "rm -rf \"$HOME/.kube\""
rm -rf "$HOME/.kube"
