#!/usr/bin/env sh

# Stop on error
set -e

# Sync kubeadm-init.yaml
scp kubeadm-init.yaml nimbus:kubeadm-init.yaml

# Run kubeadm init
ssh -t nimbus "sudo kubeadm init --config kubeadm-init.yaml"

# Copy kubeconfig
ssh -t nimbus "mkdir -p \".kube\" && sudo cp -i /etc/kubernetes/admin.conf \".kube/config\" && sudo chown \"$(id -u):$(id -g)\" \".kube/config\""
mkdir -p "$HOME/.kube"
scp nimbus:.kube/config "$HOME/.kube/config"

# Setup taints on master node
# kubectl taint node nimbus node-role.kubernetes.io/control-plane-
# kubectl taint node nimbus node-role.kubernetes.io/control-plane=:PreferNoSchedule

# Install cilium
# helm repo add cilium https://helm.cilium.io/
helm upgrade --install \
    cilium cilium/cilium \
    --namespace kube-system \
    --values helm/cilium.yaml

# Test cilium installation
cilium-cli status --wait
# cilium-cli connectivity test
# kubectl delete namespaces cilium-test-1

# Install MetalLB
# helm repo add metallb https://metallb.github.io/metallb
helm upgrade --install \
    metallb metallb/metallb \
    --namespace metallb-system \
    --create-namespace \
    --wait
# Setup MetalLB
kubectl apply -f metallb
# Delete ignore external load balancers label
kubectl label node nimbus node.kubernetes.io/exclude-from-external-load-balancers-

# Setup k8s_gateway
# helm repo add k8s_gateway https://ori-edge.github.io/k8s_gateway/
helm upgrade --install \
    exdns k8s_gateway/k8s-gateway \
    --namespace=local-dns \
    --create-namespace \
    --values helm/k8s-gateway.yaml

# Add certmanager repo
# helm repo add jetstack https://charts.jetstack.io
# Setup cert-manager
helm upgrade --install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --values helm/cert-manager.yaml
# Setup cert-manager issuers
kubectl apply -f cert-manager

# Add kubernetes-dashboard repository
# helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
# Deploy a Helm Release named "kubernetes-dashboard" using the kubernetes-dashboard chart
helm upgrade --install \
    kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard \
    --create-namespace --namespace kubernetes-dashboard \
    --values helm/kubernetes-dashboard/values.yaml

# Setup whoami
kubectl apply -f whoami
